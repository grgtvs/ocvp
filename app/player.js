// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
jQuery.widget("codejunkienet.ocvplayer", jQuery.ui.resizable, {
    options: {
        playerOptions:{
            autoFullscreen: true,
            setToBrowserLanguage: true,
            showSubtitles: true,
            triggerHTML5Events: [
                "play",
                "pause"
            ]
        }
    },
    _create: function() {
        this._super();
        this.options = $.extends({}, this.options);
        LBP.options = this.option('playerOptions');
    },
    _setOption: function(opt, value) {
        this.options[opt] = value;
    },
    _setOptions: function(opts) {
        this.options = $.extends({}, this.options, opts);
        LBP.options = $.extends({}, LBP.options, this.options.playerOptions);
    }
});

(function($){
    $("#Player").ocvplayer();
});