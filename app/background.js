

var resizeCallback = function(){
    var _cw = this.contentWindow;
    $ = _cw.jQuery;
    _newBounds = this.getBounds();
    $('#Player').attr({"width": _newBounds.width, "height": _newBounds.height});
};


chrome.runtime.onInstalled.addListener(function() {
    // Create storage data
});

chrome.runtime.onSuspend.addListener(function() {
  // Do some simple clean-up tasks.
});

chrome.app.runtime.onLaunched.addListener(function() {
    chrome.app.window.create('default.html', {
        frame: 'chrome',
        resizable: true,
        singleton: false,
        bounds: {
          width: 800,
          height: 600,
          left: 100,
          top: 100
        },
        minWidth: 800,
        minHeight: 600
    }, function(_w){
        // Bound event for the new window.
        _w.onBoundsChanged.addListener(function(){
            resizeCallback.call(_w);
        });

        _w.onClosed.addListener(function(){

        });

        _w.onFullscreened.addListener(function() {

        });

        _w.onMaximized.addListener(function() {

        });

        _w.onMinimized.addListener(function() {

        });

        _w.onRestored.addListener(function() {

        });
    });
});