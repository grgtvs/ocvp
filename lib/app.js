require.config({
    baseUrl: "lib",
    paths: {
        "jquery": "jquery/v2.0.3",
        "jqueryui": "jqueryui/v1.10.3",
        "leanbreak": "leanbreak/v0.8.0.93/js.player",
        "leanbreakExt": "leanbreak/extensions",
        "app": "../app"
    }
});

_deps = [
    "jquery",
    "jqueryui",
    "leanbreak/leanbackPlayer",
    "app/player",
    "leanbreak/leanbackPlayer.en",
    "leanbreakExt/inVideo/js.extension/leanbackPlayer.ext.invideoplaylist.pack",
    "leanbreakExt/localStorage/js.extension/leanbackPlayer.ext.localStorage.pack.js"
];

require(_deps, function(){});